import os
import sys
import torch

import utils as u
from Models import Colorizer
from api import fit, predict

#####################################################################################################

def app():
    args_1 = "--path"
    args_2 = "--model-name"
    args_3 = "--pretrained"
    args_4 = "--image-size"
    args_5 = "--expansion-factor"
    args_6 = "--bs"
    args_7 = "--lr"
    args_8 = "--wd"
    args_9 = "--epochs"
    args_10 = "--early"
    args_11 = "--scheduler"
    args_12 = "--augment"
    args_13 = "--test"
    args_14 = "--name"
    args_15 = "--big"
    args_16 = "--debug"

    data_path = "./Dataset"
    model_name = None
    pretrained = False
    image_size = 224
    expansion_factor = None
    batch_size, lr, wd = 64, 1e-3, 0
    epochs, early_stopping = 10, 5
    do_scheduler, scheduler = None, None
    do_augment = None
    train_mode = True
    name = "Test_1.jpg"
    do_big = None
    do_debug = None

    if args_1 in sys.argv: data_path = sys.argv[sys.argv.index(args_1) + 1]
    if args_2 in sys.argv: model_name = sys.argv[sys.argv.index(args_2) + 1]
    if args_3 in sys.argv: pretrained = True
    if args_4 in sys.argv: image_size = int(sys.argv[sys.argv.index(args_4) + 1])
    if args_5 in sys.argv: expansion_factor = int(sys.argv[sys.argv.index(args_5) + 1])
    if args_6 in sys.argv: batch_size = int(sys.argv[sys.argv.index(args_6) + 1])
    if args_7 in sys.argv: lr = float(sys.argv[sys.argv.index(args_7) + 1])
    if args_8 in sys.argv: wd = float(sys.argv[sys.argv.index(args_8) + 1])
    if args_9 in sys.argv: epochs = int(sys.argv[sys.argv.index(args_9) + 1])
    if args_10 in sys.argv: early_stopping = int(sys.argv[sys.argv.index(args_10) + 1])
    if args_11 in sys.argv:
        do_scheduler = True
        patience = int(sys.argv[sys.argv.index(args_11) + 1])
        eps = float(sys.argv[sys.argv.index(args_11) + 2])
    if args_12 in sys.argv: do_augment = True
    if args_13 in sys.argv: train_mode = False
    if args_14 in sys.argv: name = sys.argv[sys.argv.index(args_14) + 1]
    if args_15 in sys.argv: do_big = True
    if args_16 in sys.argv: do_debug = True

    
    assert(model_name is not None)
    torch.manual_seed(u.SEED)
    model = Colorizer(model_name=model_name, pretrained=pretrained)

    if do_debug:
        u.breaker()
        u.myprint(model, "grey")

    if train_mode:
        if do_big:
            dataloaders = u.build_dataloaders_big(path=data_path,
                                                  image_size=image_size,
                                                  batch_size=batch_size,
                                                  pretrained=pretrained)
        else:
            dataloaders = u.build_dataloaders(path=data_path, 
                                              image_size=image_size,
                                              batch_size=batch_size, 
                                              expansion_factor=expansion_factor, 
                                              pretrained=pretrained, 
                                              do_augment=do_augment)
        

        if do_debug:
            u.breaker()
            u.myprint("Train Dataloaders", "grey")
            xx, yy = next(iter(dataloaders["train"]))

            u.myprint(xx.shape, "grey")
            u.myprint(yy.shape, "grey")

            u.show_pair(xx[0].detach().cpu().numpy().transpose(1, 2, 0), 
                        yy[0].detach().cpu().numpy().transpose(1, 2, 0))

            u.breaker()
            u.myprint("Valid Dataloaders", "grey")
            xx, yy = next(iter(dataloaders["valid"]))

            u.myprint(xx.shape, "grey")
            u.myprint(yy.shape, "grey")

            u.show_pair(xx[0].detach().cpu().numpy().transpose(1, 2, 0), 
                        yy[0].detach().cpu().numpy().transpose(1, 2, 0))
        

        optimizer = model.get_optimizer(lr=lr, wd=wd)
        if do_scheduler:
            scheduler = model.get_plateau_scheduler(optimizer=optimizer, patience=patience, eps=eps)
        L, _ = fit(model=model, optimizer=optimizer, scheduler=scheduler, epochs=epochs,
                   dataloaders=dataloaders, early_stopping_patience=early_stopping, verbose=True)
        u.save_graphs(L)
    else:
        image = u.gray3(u.read_image(name))
        if pretrained:
            color_image = predict(model=model, image=image.copy(), size=image_size, transform=u.TRANSFORM_1)
        else:
            color_image = predict(model=model, image=image.copy(), size=image_size, transform=u.TRANSFORM_2)
        
        u.show_pair(image, color_image)

#####################################################################################################
