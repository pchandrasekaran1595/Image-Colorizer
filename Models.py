import re
from torch import nn, optim
from torchvision import models

#####################################################################################################

class Colorizer(nn.Module):
    def __init__(self, model_name=None, pretrained=False):
        super(Colorizer, self).__init__()

        if re.match(r"vgg", model_name, re.IGNORECASE):
            self.encoder = models.vgg16_bn(pretrained=pretrained, progress=True)
            self.encoder = nn.Sequential(*[*self.encoder.children()][:-2])
            fc = self.encoder[0][40].out_channels
        elif re.match(r"resnet", model_name, re.IGNORECASE):
            self.encoder = models.resnet50(pretrained=pretrained, progress=True)
            self.encoder = nn.Sequential(*[*self.encoder.children()][:-2])
            fc = self.encoder[7][2].conv3.out_channels
        elif re.match(r"mobilenet", model_name, re.IGNORECASE):
            self.encoder = models.mobilenet_v3_small(pretrained=pretrained, progress=True)
            self.encoder = nn.Sequential(*[*self.encoder.children()][:-2])
            fc = self.encoder[0][-1][0].out_channels
        
        if pretrained:
            self.freeze()

        self.decoder = nn.Sequential()
        self.decoder.add_module("DC1", nn.ConvTranspose2d(in_channels=fc, out_channels=512, kernel_size=4, stride=2, padding=1))
        self.decoder.add_module("AN1", nn.ReLU())
        self.decoder.add_module("DC2", nn.ConvTranspose2d(in_channels=512, out_channels=256, kernel_size=2, stride=2))
        self.decoder.add_module("AN2", nn.ReLU())
        self.decoder.add_module("DC3", nn.ConvTranspose2d(in_channels=256, out_channels=128, kernel_size=2, stride=2))
        self.decoder.add_module("AN3", nn.ReLU())
        self.decoder.add_module("DC4", nn.ConvTranspose2d(in_channels=128, out_channels=64, kernel_size=2, stride=2))
        self.decoder.add_module("AN4", nn.ReLU())
        self.decoder.add_module("DC5", nn.ConvTranspose2d(in_channels=64, out_channels=3, kernel_size=2, stride=2))
        self.decoder.add_module("AN5", nn.ReLU())

    def freeze(self):
        for params in self.parameters():
            params.requires_grad = False
        
    def get_optimizer(self, lr=1e-3, wd=0):
        params = [p for p in self.parameters() if p.requires_grad]
        return optim.Adam(params, lr=lr, weight_decay=wd)
    
    def get_plateau_scheduler(self, optimizer=None, patience=None, eps=None):
        assert(optimizer is not None and patience is not None and eps is not None)
        return optim.lr_scheduler.ReduceLROnPlateau(optimizer=optimizer, patience=patience, eps=eps, verbose=True)
    
    def forward(self, x):
        encoded = self.encoder(x)
        decoded = self.decoder(encoded)

        return encoded, decoded

#####################################################################################################
