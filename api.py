import os
import re
import cv2
import torch
import numpy as np
from time import time

import utils as u

#####################################################################################################

def fit(model=None, optimizer=None, scheduler=None, epochs=None,
        dataloaders=None, early_stopping_patience=None, verbose=False):
    
    u.breaker()
    u.myprint("Training ...", "cyan")
    u.breaker()

    Losses = []
    bestLoss = {"train" : np.inf, "valid" : np.inf}

    model.to(u.DEVICE)
    start_time = time()
    for e in range(epochs):
        e_st = time()
        epochLoss = {"train" : np.inf, "valid" : np.inf}

        for phase in ["train", "valid"]:
            if phase == "train":
                model.train()
            else:
                model.eval()
            
            lossPerPass = []

            for bw_image, color_image in dataloaders[phase]:
                bw_image, color_image = bw_image.to(u.DEVICE), color_image.to(u.DEVICE)

                optimizer.zero_grad()
                with torch.set_grad_enabled(phase == "train"):
                    _, output = model(bw_image)
                    loss = torch.nn.MSELoss()(output, color_image)
                    if phase == "train":
                        loss.backward()
                        optimizer.step()
                lossPerPass.append(loss.item())
            epochLoss[phase] = np.mean(np.array(lossPerPass))
        Losses.append(epochLoss)

        if early_stopping_patience:
            if epochLoss["valid"] < bestLoss["valid"]:
                bestLoss = epochLoss
                BLE = e + 1
                torch.save({"model_state_dict": model.state_dict(),
                            "optim_state_dict": optimizer.state_dict()},
                           os.path.join(u.CHECKPOINT_PATH, "state.pt"))
                early_stopping_step = 0
            else:
                early_stopping_step += 1
                if early_stopping_step > early_stopping_patience:
                    u.myprint("\nEarly Stopping at Epoch {}".format(e + 1), "green")
                    break
        
        if epochLoss["valid"] < bestLoss["valid"]:
            bestLoss = epochLoss
            BLE = e + 1
            torch.save({"model_state_dict" : model.state_dict(),
                        "optim_state_dict" : optimizer.state_dict()},
                        os.path.join(u.CHECKPOINT_PATH, "state.pt"))
        
        if scheduler:
            scheduler.step(epochLoss["valid"])
        
        if verbose:
            u.myprint("Epoch: {} | Train Loss: {:.5f} | Valid Loss: {:.5f} | Time: {:.2f} seconds".format(e+1, 
                                                                       epochLoss["train"], epochLoss["valid"], 
                                                                       time()-e_st), "cyan")
    
    u.breaker()
    u.myprint("Best Validation Loss at Epoch {}".format(BLE), "cyan")
    u.breaker()
    u.myprint("Time Taken [{} Epochs] : {:.2f} minutes".format(len(Losses), (time()-start_time)/60), "cyan")
    u.breaker()
    u.myprint("Training Completed", "cyan")
    u.breaker()

    return Losses, BLE

#####################################################################################################

def predict_batch(model=None, dataloader=None, size=None, mode="test"):
    model.load_state_dict(torch.load(os.path.join(u.CHECKPOINT_PATH, "state.pt"), map_location=u.DEVICE)["model_state_dict"])
    model.to(u.DEVICE)
    model.eval()

    y_pred = torch.zeros(1, 3, size, size).to(u.DEVICE)
    if re.match(r"valid", mode, re.IGNORECASE):
        for bw_image, _ in dataloader:
            with torch.no_grad():
                _, output = torch.sigmoid(model(bw_image))
            y_pred = torch.cat((y_pred, output), dim=0)
    elif re.match(r"test", mode, re.IGNORECASE):
        for bw_image in dataloader:
            with torch.no_grad():
                output = torch.sigmoid(model(bw_image))
            y_pred = torch.cat((y_pred, output), dim=0)
    
    return y_pred[1:].detach().cpu().numpy().transpose(0, 2, 3, 1)

#####################################################################################################

def predict(model=None, image=None, size=None, transform=None):
    h, w, _ = image.shape
    image = cv2.resize(src=image, dsize=(size, size), interpolation=cv2.INTER_AREA)

    model.load_state_dict(torch.load(os.path.join(u.CHECKPOINT_PATH, "state.pt"), map_location=u.DEVICE)["model_state_dict"])
    model.to(u.DEVICE)
    model.eval()

    with torch.no_grad():
        _, color_image = model(transform(image).unsqueeze(dim=0).to(u.DEVICE))
    color_image = torch.sigmoid(color_image.squeeze())
    color_image = color_image.detach().cpu().numpy().transpose(1, 2, 0)
    color_image = np.clip((color_image*255), 0, 255).astype("uint8")

    return cv2.resize(src=color_image, dsize=(w, h), interpolation=cv2.INTER_AREA)

#####################################################################################################

