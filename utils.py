import os
import re
import cv2
import torch
import imgaug
import numpy as np
import matplotlib.pyplot as plt

from termcolor import colored
from imgaug import augmenters
from torchvision import transforms
from torch.utils.data import Dataset
from torch.utils.data import DataLoader as DL
from sklearn.model_selection import KFold

os.system("color")

#####################################################################################################

def myprint(text: str, color:str) -> None:
    print(colored(text=text, color=color))


def breaker(num=50, char="*"):
    myprint("\n" + num*char + "\n", "magenta")

#####################################################################################################

class DS(Dataset):
    def __init__(self, bw_images=None, color_images=None, transform=None, mode="train"):
        self.mode = mode
        
        assert(re.match(r"train", self.mode, re.IGNORECASE) or re.match(r"valid", self.mode, re.IGNORECASE) or re.match(r"test", self.mode, re.IGNORECASE))
        
        self.transform = transform
        self.bw_images = bw_images
        if re.match(r"train", self.mode, re.IGNORECASE) or re.match(r"valid", self.mode, re.IGNORECASE):
            self.color_images = color_images

    def __len__(self):
        return self.bw_images.shape[0]
    
    def __getitem__(self, idx):
        if re.match(r"train", self.mode, re.IGNORECASE) or re.match(r"valid", self.mode, re.IGNORECASE):
            return self.transform(self.bw_images[idx]), self.transform(self.color_images[idx])
        else:
            return self.transform(self.bw_images[idx])


class BigDS(Dataset):
    def __init__(self, path=None, filenames=None, size=None, transform=None, mode="train"):
        self.mode = mode

        assert(re.match(r"train", self.mode, re.IGNORECASE) or re.match(r"valid", self.mode, re.IGNORECASE) or re.match(r"test", self.mode, re.IGNORECASE))

        self.path = path
        self.filenames = filenames
        self.size = size
        self.transform = transform

    def __len__(self):
        return self.filenames.shape[0]

    def __getitem__(self, idx):
        image = preprocess(cv2.imread(os.path.join(self.path, self.filenames[idx]), cv2.IMREAD_COLOR), size=self.size)
        if re.match(r"train", self.mode, re.IGNORECASE) or re.match(r"valid", self.mode, re.IGNORECASE):
            return self.transform(gray3(image.copy(), from_rgb=True)), self.transform(image)
        else:
            # Will need a Fix
            return self.transform(gray3(image.copy()))
        

#####################################################################################################

def read_image(name: str) -> np.ndarray:
    image = cv2.imread(os.path.join(TEST_DATA_PATH, name), cv2.IMREAD_GRAYSCALE)
    assert(image is not None)
    return image


def preprocess(image: np.ndarray, size: int) -> np.ndarray:
    return cv2.resize(src=cv2.cvtColor(src=image, code=cv2.COLOR_BGR2RGB), 
                      dsize=(size, size), 
                      interpolation=cv2.INTER_AREA)


def gray3(image: np.ndarray, from_rgb=False) -> np.ndarray:
    if from_rgb:
        return cv2.cvtColor(src=cv2.cvtColor(src=image, code=cv2.COLOR_RGB2GRAY), code=cv2.COLOR_GRAY2RGB)
    else:
        return cv2.cvtColor(src=image, code=cv2.COLOR_GRAY2RGB)


def show(image: np.ndarray, title=None) -> None:
    plt.figure()
    if title:
        plt.title(title)
    plt.imshow(image)
    plt.axis("off")
    plt.show()


def show_pair(image_1: np.ndarray, image_2: np.ndarray) -> None:
    plt.figure()
    plt.subplot(1, 2, 1)
    plt.imshow(image_1)
    plt.title("BW Image")
    plt.axis("off")
    plt.subplot(1, 2, 2)
    plt.imshow(image_2)
    plt.title("Color Image")
    plt.axis("off")
    plt.show()

#####################################################################################################

def get_augment(seed: int):
    imgaug.seed(seed)
    augment = augmenters.SomeOf(None, [
        augmenters.HorizontalFlip(p=0.5),
        augmenters.VerticalFlip(p=0.5),
        augmenters.Affine(scale=(0.75, 1.25), translate_percent=(-0.1, 0.1), rotate=(-45, 45), seed=seed),
    ], seed=seed)
    return augment


def get_images(path: str, size: int) -> np.ndarray:
    # names = [files for files in os.listdir(path) if files[-3:]=="jpg"]
    # bw_images    = np.zeros((len(names), size, size, 3)).astype("uint8")
    # color_images = np.zeros((len(names), size, size, 3)).astype("uint8")

    bw_images    = np.zeros((len(os.listdir(path)), size, size, 3)).astype("uint8")
    color_images = np.zeros((len(os.listdir(path)), size, size, 3)).astype("uint8")
    i = 0
    # for filename in names:
    for filename in os.listdir(path):
        image = preprocess(cv2.imread(os.path.join(path, filename), cv2.IMREAD_COLOR), size=size)
        bw_images[i] = gray3(image.copy(), from_rgb=True)
        color_images[i] = image
        i += 1
    return bw_images, color_images


def make_synthetic_data(images: np.ndarray, factor: int) -> np.ndarray:
    images = list(images) * factor
    return np.array(images)


def build_dataloaders(path: str, image_size: int, batch_size: int, expansion_factor=None, pretrained=False, do_augment=False):
    breaker()
    myprint("Reading Images ...", "yellow")
    bw_images, color_images = get_images(path, size=image_size)

    if expansion_factor:
        breaker()
        myprint("Expanding Dataset ...", "yellow")
        bw_images = make_synthetic_data(bw_images, factor=expansion_factor)
        color_images = make_synthetic_data(color_images, factor=expansion_factor)
    
    for tr_idx, va_idx in KFold(n_splits=5, shuffle=True, random_state=SEED).split(bw_images):
        tr_bw_images, va_bw_images = bw_images[tr_idx], bw_images[va_idx]
        tr_color_images, va_color_images = color_images[tr_idx], color_images[va_idx]
        break

    if do_augment:
        breaker()
        myprint("Augmenting Training Data ...", "yellow")
        augment = get_augment(seed=SEED)
        tr_bw_images = augment(images=tr_bw_images)

        augment = get_augment(seed=SEED)
        tr_color_images = augment(images=tr_color_images)

    breaker()
    myprint("Building Dataloaders ...", "yellow")
    if pretrained:
        tr_data_setup = DS(bw_images=tr_bw_images, color_images=tr_color_images, transform=TRANSFORM_1)
        va_data_setup = DS(bw_images=va_bw_images, color_images=va_color_images, transform=TRANSFORM_1)
    else:
        tr_data_setup = DS(bw_images=tr_bw_images, color_images=tr_color_images, transform=TRANSFORM_2)
        va_data_setup = DS(bw_images=va_bw_images, color_images=va_color_images, transform=TRANSFORM_2)
    
    dataloaders = {
        "train" : DL(tr_data_setup, batch_size=batch_size, shuffle=True, generator=torch.manual_seed(SEED)),
        "valid" : DL(va_data_setup, batch_size=batch_size, shuffle=False)
    }

    return dataloaders


def build_dataloaders_big(path: str, image_size: int, batch_size: int, pretrained=False):
    filenames = np.array(os.listdir(path))

    for tr_idx, va_idx in KFold(n_splits=5, shuffle=True, random_state=SEED).split(filenames):
        tr_filenames, va_filenames = filenames[tr_idx], filenames[va_idx]
        break

    breaker()
    myprint("Building Dataloaders ...", "yellow")
    if pretrained:
        tr_data_setup = BigDS(path=path, filenames=tr_filenames, size=image_size, transform=TRANSFORM_1, mode="train")
        va_data_setup = BigDS(path=path, filenames=va_filenames, size=image_size, transform=TRANSFORM_1, mode="valid")
    else:
        tr_data_setup = BigDS(path=path, filenames=tr_filenames, size=image_size, transform=TRANSFORM_2, mode="train")
        va_data_setup = BigDS(path=path, filenames=va_filenames, size=image_size, transform=TRANSFORM_2, mode="valid")
    
    dataloaders = {
        "train" : DL(tr_data_setup, batch_size=batch_size, shuffle=True, generator=torch.manual_seed(SEED)),
        "valid" : DL(va_data_setup, batch_size=batch_size, shuffle=False)
    }

    return dataloaders

#####################################################################################################

def save_graphs(L: list) -> None:
    TL, VL = [], []
    for i in range(len(L)):
        TL.append(L[i]["train"])
        VL.append(L[i]["valid"])
    x_Axis = np.arange(1, len(TL) + 1)
    plt.figure("Plots")
    plt.plot(x_Axis, TL, "r", label="Train")
    plt.plot(x_Axis, VL, "b", label="Valid")
    plt.legend()
    plt.grid()
    plt.title("Loss Graph")
    plt.savefig("./Graphs.jpg")
    plt.close("Plots")

#####################################################################################################

SEED = 0
DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")
CHECKPOINT_PATH = "./Checkpoints"
TEST_DATA_PATH  = "./Test Images"
if not os.path.exists(CHECKPOINT_PATH):
    os.makedirs(CHECKPOINT_PATH)
TRANSFORM_1 = transforms.Compose([transforms.ToTensor(), 
                                transforms.Normalize(mean=[0.485, 0.456, 0.406], 
                                                       std=[0.229, 0.224, 0.225])])
TRANSFORM_2 = transforms.Compose([transforms.ToTensor(), ])

#####################################################################################################
