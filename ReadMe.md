Black and White Image Colorization using Deep Convolutional Neural Networks (AutoEncoders). 

---

&nbsp;

### **CLI Arguments**
<pre>
1. --path             - Dataset Path (Default: ./Dataset)
2. --model-name       - Name of the model (Supports 'resnet', 'vgg' & 'mobilenet')
3. --pretrained       - Flag that controls whether to use a pretrained model (Default: None)
4. --image-size       - Image Size fed as input to the CNN (Default: 224) 
5. --expansion-factor - Total Images = Expansion Factor * No. of Images in Dataset Folder (Default: None)
6. --bs               - Batch Size (Default: 64)
7. --lr               - Learing Rate (Default: 1e-3)
8. --wd               - Weight Decay (Default: 0)
9. --epochs           - Number of training epochs (Default: 10)
10. --early           - Early stopping patience (Default: 5)
11. --scheduler       - Needs two arguments; patience and eps
12. --augment         - Flag that controls train set augmentation (Default: None)
13. --test            - Flag that controls entry into test mode (Default: False)
14. --name            - Name of the image file to be tested (Default: Test_1.jpg)
15. --big             - Flag useful when handling very large amount of images

</pre>

&nbsp;

---

&nbsp;

1. If `--path` isn't specified, it looks for images in the default Dataset folder
2. When using `--pretrained`, do not specify `image-size`
3. Data Acquisition will need modification to handle very large datasets (Add Fix + CLI Arg)

&nbsp;

---

